class Reader::ProjectsController < Projects::ApplicationController
  include ExtractsPath

  skip_before_action nil
  before_action :project
  before_action :repository
  layout 'project'

  before_action :assign_ref_vars, only: [:show]
  before_action :tree, only: [:show]

  def tree
    @tree ||= @repository.tree(@commit.id, @path)
  end

  def show
    respond_to do |format|
      format.html
    end
  end

end
