class Reader::UserController < ::UsersController
  before_action :user, except: [:exists]

  def index
    load_projects

    respond_to do |format|
      format.html
    end
  end

  def load_projects
    @projects =
        PersonalProjectsFinder.new(user).execute(current_user)
            .page(params[:page])
  end

  def exists
    render json: { exists: !!Namespace.find_by_path_or_name(params[:username]) }
  end

  def user
    @user ||= User.find_by_username!(params[:username])
  end

end
