scope module: :reader do
  # resource :projects, constraints: { username: /[a-zA-Z.0-9_\-]+/}, path: '~:username', as: :user, only: [ :index, :show ]
  get '~:username', controller: :user, action: :index
  get '~:namespace_id/:id', controller: :projects, action: :show
end
