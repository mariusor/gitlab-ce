# Class to parse and transform the info provided by omniauth
#
module Gitlab
  module Reddit
    class AuthHash < Gitlab::OAuth::AuthHash
      def initialize(auth_hash)
        super
      end

      def username_and_email
        @username_and_email ||= begin
          username  = get_info(:name).presence || get_info(:username).presence || get_info(:nickname).presence
          email     ||= generate_temporarily_email(username)

          {
            username: username,
            email:    email
          }
        end
      end

      def generate_temporarily_email(username)
        "#{username}@reddit.com"
      end
    end
  end
end
