require 'gitlab/o_auth/user'

# Reddit extension for User model
#
# * Find or create user from omniauth.auth data
# * Creates an invalid @reddit.com email
#
module Gitlab
  module Reddit
    class User < Gitlab::OAuth::User

      def initialize(auth_hash)
        super
      end

      def save
        super('reddit')
      end

      def changed?
        return true unless gl_user
        gl_user.changed? || gl_user.identities.any?(&:changed?)
      end

      def gl_user
        @user ||= find_by_uid_and_provider

        if signup_enabled?
          @user ||= build_new_user
        end

        if external_provider? && @user
          @user.external = true
        end

        @user
      end

      def auth_hash=(auth_hash)
        @auth_hash = Gitlab::Reddit::AuthHash.new(auth_hash)
      end
    end
  end
end
